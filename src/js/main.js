/*
 Third party
 */
//= ../../bower_components/jquery/dist/jquery.min.js

/*
    Custom
 */
//= partials/helper.js

$(function() {
	$("img, a").on("dragstart", function(event) { event.preventDefault(); });

    $('#navbarMenu').collapse({
        toggle: false
    })

    $('#main-slider').carousel();
    $('.collapse').collapse();

    $(".menu-scroll").on("click", "a", function (event) {
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();

        //забираем идентификатор блока с атрибута href
        var id = $(this).attr('href'),

            //узнаем высоту от начала страницы до блока на который ссылается якорь
            top = $(id).offset().top;

        //анимируем переход на расстояние - top за 1500 мс
        $('body,html').animate({ scrollTop: top }, 1500);
    });

    // scroll up
    // $('#up').hide();
    var scrollUp = document.getElementById('up');
    window.onscroll = function () { // при скролле показывать и прятать блок
        if (window.pageYOffset > 500) {
            scrollUp.style.display = 'block';
        } else {
            scrollUp.style.display = 'none';
        }
    };
    $('#up').click(function () {
        $('html, body').animate({ scrollTop: 0 }, 500);
        return false;
    });

    var toggle = document.querySelector('.menu-btn');

    toggle.addEventListener('click', function (e) {
        this.classList.toggle('opened');
    });
    
    $(".site-overlay").click(function(){
        toggle.classList.toggle('opened');
    });

});