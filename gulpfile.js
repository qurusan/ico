'use strict';

require('es6-promise').polyfill();

var gulp = require('gulp'),
	watch = require('gulp-watch'),
	pug = require('gulp-pug'), //pug
	sass = require('gulp-sass'),
	prefixer = require('gulp-autoprefixer'),
	spritesmith  = require('gulp.spritesmith'),
	uglify = require('gulp-uglify'),
	// sourcemaps = require('gulp-sourcemaps'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	browserSync = require("browser-sync").create(),
	// reload = browserSync.reload;
	cssnano = require('gulp-cssnano'), // `gulp-minify-css` depreciated. Use `gulp-cssnano`
	jshint = require('gulp-jshint'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'), //конкатенация файлов
	notify = require('gulp-notify'),
	plumber = require('gulp-plumber'),
	cached = require('gulp-cached'),
	changed = require('gulp-changed'),
	newer = require('gulp-newer'),
	// ftp = require('vinyl-ftp'),
	// rsync = require('gulp-rsync'),
	del = require('del');


gulp.task('webserver', ['sass:build', 'js:build', 'pug:build'], function() {
	browserSync.init({
		// proxy: 'site.dev',
		server: {
			baseDir: "app/"
		},
		notify: false,
		port: 8091
	});
});

// pug
gulp.task('pug:build', function () {
	return gulp.src('src/pug/*.pug')
	.pipe(plumber({errorHandler: notify.onError("pug error: <%= error.message %>")}))
	.pipe(pug({
		pretty: true
	}))
	.pipe(gulp.dest('app/'));
	// .pipe(reload({stream: true}));
});

// Styles
gulp.task('sass:build', function () {
	return gulp.src('src/sass/*.sass')
	.pipe(plumber({errorHandler: notify.onError("Sass error: <%= error.message %>")}))
	// .pipe(sourcemaps.init())
	.pipe(sass({
		includePaths: ['src/sass/'],
		outputStyle: 'expanded',
		sourceMap: false,
		errLogToConsole: true
	}).on('error', sass.logError))
	.pipe(prefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
	// .pipe(sourcemaps.write())
	.pipe(gulp.dest('app/assets/css/'))
	// .pipe(rename({ suffix: '.min' }))
	// .pipe(cssnano())
	.pipe(gulp.dest('app/assets/css/'))
	.pipe(notify({ message: 'Styles task complete' }))
	.pipe(browserSync.stream());
});

gulp.task('sass-lk:build', function () {
	return gulp.src('src/sass-lk/*.sass')
		.pipe(plumber({ errorHandler: notify.onError("Sass error: <%= error.message %>") }))
		// .pipe(sourcemaps.init())
		.pipe(sass({
			includePaths: ['src/sass-lk/'],
			outputStyle: 'expanded',
			sourceMap: false,
			errLogToConsole: true
		}).on('error', sass.logError))
		.pipe(prefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
		// .pipe(sourcemaps.write())
		.pipe(gulp.dest('app/assets/css/'))
		// .pipe(rename({ suffix: '.min' }))
		// .pipe(cssnano())
		.pipe(gulp.dest('app/assets/css/'))
		.pipe(notify({ message: 'Styles task complete' }))
		.pipe(browserSync.stream());
});

// Copy sass
gulp.task('sass:copy', function(){
	gulp.src('src/sass/**/*.*').pipe(gulp.dest('app/assets/sass/'))
});

// Scripts
gulp.task('js:build', function () {
	return gulp.src(["src/js/jquery-3.2.1.slim.min.js", "src/js/bootstrap.js", "src/js/partials/**.js", "src/js/main.js"])
	.pipe(plumber({errorHandler: notify.onError("Script error: <%= error.message %>")}))
	// .pipe(jshint('.jshintrc'))
	// .pipe(jshint.reporter('default'))
	// .pipe(concat('jquery-3.2.1.slim.min.js'))
		// .pipe(concat(["jquery-3.2.1.slim.min.js", "bootstrap.js"]))
	.pipe(concat('main.js'))
	// .pipe(sourcemaps.init())
	// .pipe(sourcemaps.write())
	.pipe(gulp.dest('app/assets/js'))
	// .on('error', gutil.log)
	// .pipe(rigger())
	.pipe(rename({ suffix: '.min' }))
	// .pipe(sourcemaps.init())
	.pipe(uglify())
	// .pipe(sourcemaps.write())
	.pipe(gulp.dest('app/assets/js'))
	// .on('error', gutil.log)
	.pipe(notify({ message: 'Scripts task complete' }));
	// .pipe(reload({stream: true}));
});

// Sprite
gulp.task('sprite:build', function() {
	var spriteData = gulp.src('src/sprite/*.*') // путь, откуда берем картинки для спрайта
		.pipe(plumber({errorHandler: notify.onError("Sprite error: <%= error.message %>")}))
		.pipe(spritesmith({
			imgName: 'sprite.png',
			cssName: '_sprite.sass',
			imgPath : '../sprite/sprite.png',
			cssFormat: 'sass',
			algorithm:  'top-down',
			padding: 10,
			cssVarMap: function(sprite) {
				sprite.name = 's-' + sprite.name
			}
		}));

	spriteData.img.pipe(gulp.dest('./app/assets/sprite/')); // путь, куда сохраняем картинку
	spriteData.css.pipe(gulp.dest('./src/sass/partials/')); // путь, куда сохраняем стили
});

// Sprite
gulp.task('sprite-lk:build', function () {
	var spriteData = gulp.src('src/sprite-lk/*.*') // путь, откуда берем картинки для спрайта
		.pipe(plumber({ errorHandler: notify.onError("Sprite error: <%= error.message %>") }))
		.pipe(spritesmith({
			imgName: 'sprite-lk.png',
			cssName: '_sprite-lk.sass',
			imgPath: '../sprite/sprite-lk.png',
			cssFormat: 'sass',
			algorithm: 'top-down',
			padding: 10,
			cssVarMap: function (sprite) {
				sprite.name = 's-' + sprite.name
			}
		}));

	spriteData.img.pipe(gulp.dest('./app/assets/sprite/')); // путь, куда сохраняем картинку
	spriteData.css.pipe(gulp.dest('./src/sass-lk/partials/')); // путь, куда сохраняем стили
});

// Images
gulp.task('image:build', function () {
	return gulp.src('src/img/**/*.*')
	.pipe(newer('app/img/'))
	.pipe(imagemin({
		optimizationLevel: 3,
		progressive: true,
		svgoPlugins: [{removeViewBox: false}],
		use: [pngquant()],
		interlaced: true
	}))
	.pipe(gulp.dest('app/assets/img/'))
	.pipe(notify({ message: 'Images task complete' }));
	// .pipe(reload({stream: true}));
});

// Fonts
gulp.task('fonts:build', function() {
	return gulp.src('src/fonts/**/*.*')
	.pipe(gulp.dest('app/assets/fonts/'))
});

// // css-libs
// gulp.task('js-libs', function() {
// 	return gulp.src([ // Берем все необходимые библиотеки
// 		'app/assets/libs/jquery/dist/jquery.min.js', // Берем jQuery
// 		'app/assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js' // Берем Magnific Popup
// 		])
// 		.pipe(concat('libs.min.js')) // Собираем их в кучу в новом файле libs.min.js
// 		.pipe(uglify()) // Сжимаем JS файл
// 		.pipe(gulp.dest('app/assets/js')); // Выгружаем в папку app/assets/js
// });

// // css-libs
// gulp.task('css-libs', ['sass'], function() {
// 	return gulp.src('app/assets/css/libs.css') // Выбираем файл для минификации
// 		.pipe(cssnano()) // Сжимаем
// 		.pipe(rename({suffix: '.min'})) // Добавляем суффикс .min
// 		.pipe(gulp.dest('app/assets/css')); // Выгружаем в папку app/css
// });

// Clean
gulp.task('clean', function() {
	// return del(['dist/styles', 'dist/scripts', 'dist/images']);
	// return del.sync('dist');
	return del(['app/**']);
});

// Clear cache
gulp.task('clearcache', function (callback) {
	return cache.clearAll();
});

gulp.task('build', [
	'pug:build',
	'js:build',
	'sass:build',
	'sass-lk:build',
	'sass:copy',
	'fonts:build',
	'image:build',
	'sprite:build',
	'sprite-lk:build'
]);

gulp.task('watch', function(){
	watch(['src/pug/**/*.pug'], function(event, cb){
		gulp.start('pug:build');
	});
	watch(['src/sass/**/*.sass'], function(event, cb) {
		gulp.start('sass:build');
	});
	watch(['src/sass-lk/**/*.sass'], function(event, cb) {
		gulp.start('sass-lk:build');
	});
	watch(['src/js/**/*.js'], function(event, cb) {
		gulp.start('js:build');
	});
	watch(['src/img/**/*.*'], function(event, cb) {
		gulp.start('image:build');
	});
	watch(['src/fonts/**/*.*'], function(event, cb) {
		gulp.start('fonts:build');
	});

	watch(['src/sprite/*.*'], function(event, cb){
		gulp.start('sprite:build')
	});
	watch(['src/sprite-lk/*.*'], function(event, cb){
		gulp.start('sprite-lk:build')
	});

	watch('app/assets/js/*.js').on('change', browserSync.reload);
	watch('app/*.html').on('change', browserSync.reload);
	watch('app/assets/css/*.css').on('change', browserSync.reload);

});


gulp.task('default', ['build', 'webserver', 'watch']);

// Deploy
// gulp.task('deploy', function() {

// 	var conn = ftp.create({
// 		host:      'hostname.com',
// 		user:      'username',
// 		password:  'userpassword',
// 		parallel:  10,
// 		log: gutil.log
// 	});

// 	var globs = [
// 	'dist/**',
// 	'dist/.htaccess',
// 	];
// 	return gulp.src(globs, {buffer: false})
// 	.pipe(conn.dest('/path/to/folder/on/server'));

// });

// RSync
// gulp.task('rsync', function() {
// 	return gulp.src('dist/**')
// 	.pipe(rsync({
// 		root: 'dist/',
// 		hostname: 'username@yousite.com',
// 		destination: 'yousite/public_html/',
// 		archive: true,
// 		silent: false,
// 		compress: true
// 	}));
// });


// gulp.task('scripts', function() {
//  return gulp.src([
//      './app/libs/modernizr/modernizr.js',
//      './app/libs/jquery/jquery-1.11.2.min.js',
//      './app/libs/waypoints/waypoints.min.js',
//      './app/libs/animate/animate-css.js',
//      './app/libs/plugins-scroll/plugins-scroll.js',
//      ])
//      .pipe(concat('libs.js'))
//      // .pipe(uglify()) //Minify libs.js
//      .pipe(gulp.dest('./app/js/'));
// });


/*!
 * gulp
 * $ npm install es6-promise gulp-pug gulp-sass gulp-watch browser-sync imagemin-pngquant gulp.spritesmith gulp-autoprefixer gulp-cssnano gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-notify gulp-rename gulp-cache del --save-dev
 */